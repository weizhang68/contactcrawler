package com.bgitechgroup.nkba;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ContactTest {
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void TestToString() throws Exception {
        String expected = "CARLETON L. ELLSWORTH,CARLETON,ELLSWORTH," +
                "Designer/Fabricator," +
                "AFFINITY STONEWORKS,(770) 346-9888," +
                "carleton@affinitystoneworks.com," +
                "3005 Trotters Pkwy| Alpharetta| GA,N/A";
        Contact contact = Contact.makeContact(
                "CARLETON L. ELLSWORTH",
                "Designer/Fabricator",
                "AFFINITY STONEWORKS",
                "(770) 346-9888",
                "carleton@affinitystoneworks.com",
                "3005 Trotters Pkwy| Alpharetta| GA",
                "N/A");
        assertEquals("Should be equal", expected, contact.toString());
    }
}