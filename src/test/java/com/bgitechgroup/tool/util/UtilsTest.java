package com.bgitechgroup.tool.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtilsTest {
    final static String PROJ_DIR = System.getProperty("user.dir");

    @Test
    public void testExtactDomainFromEmail() throws Exception {
        String email = "abc@foo.bar.com";
        String expected = "foo.bar.com";
        String actual = Utils.extractDomainFromEmail(email);
        assertEquals("Should be equal", expected, actual);
    }

    @Test
    public void testgGenerateFileName() throws Exception {
        String state = "GA";
        String expected = PROJ_DIR + "/" + Utils.OUTPUT_DIR + "/" + Utils.FILE_PREFIX + "-" + state + "-";
        String actual = Utils.generateFileName(state);
        int len = expected.length();
        assertEquals("Should start with", expected, actual.substring(0, len));
    }

    @Test
    public void testgNormalize() throws Exception {
        String s = "ABC, INC.";
        String expected = "\"ABC, INC.\"";
        String actual = Utils.normalize(s);

        assertEquals("Shold be equal", expected, actual);


    }

}