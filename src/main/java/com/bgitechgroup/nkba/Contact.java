package com.bgitechgroup.nkba;


import com.bgitechgroup.tool.util.Utils;

public class Contact {
    public static final String NOT_APPLICABLE = "N/A";
    private String ownerFullName;
    private String category;
    private String companyName;
    private String Phone;
    private String email;
    private String address;
    private String web;

    public Contact(String ownerFullName, String category, String companyName,
                   String phone, String email, String address, String web) {

        ownerFullName = ownerFullName.trim();
        // Use company name if the owner full name is not present
        if (ownerFullName.isEmpty()) {
            ownerFullName = companyName.trim();
        }
        // Use the first part if the owner full name contains comma.
        if (ownerFullName.contains(",")) {
            String[] tokens = ownerFullName.trim().split(",");
            ownerFullName = tokens[0];
        }

        this.ownerFullName = Utils.normalize(ownerFullName);
        this.companyName = Utils.normalize(companyName);
        this.category = Utils.normalize(category);
        Phone = Utils.normalize(phone);
        this.email = Utils.normalize(email);
        this.address = Utils.normalize(address);
        this.web = Utils.normalize(web);
    }

    public static Contact makeContact(
            String ownerName, String category, String companyName, String phone,
            String email, String address, String web) {

        return new Contact(ownerName, category, companyName, phone, email,
                address, web);
    }

    /**
     * @return scv
     */
    @Override
    public String toString() {
        String ownerFirstName, ownerLastName;
        if (ownerFullName.equals(NOT_APPLICABLE)) {
            ownerFullName = NOT_APPLICABLE;
            ownerFirstName = "N/A";
            ownerLastName = "N/A";
        } else {
            String[] names = ownerFullName.split(" ");
            int len = names.length;
            if (len == 1) {
                ownerFirstName = names[0];
                ownerLastName = names[0];
            } else if (len == 2) {
                ownerFirstName = names[0];
                ownerLastName = names[1];
            } else {
                ownerFirstName = names[0];
                ownerLastName = names[2];
            }
        }

        return String.join(",", ownerFullName, ownerFirstName, ownerLastName,
                category, companyName, Phone, email, address, web);
    }
}
