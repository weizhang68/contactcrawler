package com.bgitechgroup.nkba;

import com.bgitechgroup.tool.util.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.bgitechgroup.tool.util.Utils.buildSetFromCvs;

public class ContactGrabber {
    final static String PROJ_DIR = System.getProperty("user.dir");

    final static String TARGET_URL = "http://www.nkba.org/Design/Homeowners/ProSearch.aspx/";

    final Set<String> exclusiveDomains;
    private final Set<String> processedEmails;


    int exclusiveCount = 0;
    int duplicateCount = 0;


    public static void main(String[] args) {
        ContactGrabber grabber = new ContactGrabber();
        grabber.grabeContacts();
    }

    public ContactGrabber() {
        processedEmails = buildSetFromCvs(PROJ_DIR + "/data/processed_emails.csv");
        exclusiveDomains = buildSetFromCvs(PROJ_DIR + "/data/exclude-domains.csv");
        System.out.printf("\n# of processedEmails: %d\n\n", processedEmails.size());
    }

    public void grabeContacts() {
        String[] states = {
                "SC",
                "NC"
        };
        try {
            for (String state : states) {
                System.out.printf("\nProcessing state [%s] ... ... \n\n", state);
                String sourceFileName = PROJ_DIR + "/data/" + state + "_Cities.csv";
                Set<String> stateCities = Utils.buildSetFromCvs(sourceFileName);
                String contactFileName = Utils.generateFileName(state);
                Path contactPath = Paths.get(contactFileName);
                String emailFileName = PROJ_DIR + "/data/processed_emails.csv";
                Path emailPath = Paths.get(emailFileName);
                String skippedStateCityFileName = PROJ_DIR + "/data/skipped_state_cities.csv";
                Path skippedStateCityPath = Paths.get(skippedStateCityFileName);

                try (BufferedWriter contactWriter = Files.newBufferedWriter(contactPath);
                     BufferedWriter emailWriter = Files.newBufferedWriter(emailPath, StandardOpenOption.APPEND);
                     BufferedWriter skippedStateCityWriter = Files.newBufferedWriter(skippedStateCityPath, StandardOpenOption.APPEND);
                ) {
                    // Add header
                    contactWriter.write(Utils.makeContactHeader());
                    contactWriter.newLine();

                    for (String city : stateCities) {
                        System.out.printf("\n\tProcessing state/city [%s/%s] ... ... \n\n", state, city);
                        grabeContacts(state, city, contactWriter, emailWriter, skippedStateCityWriter);
                        System.out.printf("\n\tOK\n\n");
                    }
                }

                System.out.printf("\nOK\n\n");
            }
        } catch (IOException e) {
            // TODO: log
            e.printStackTrace();
            System.out.printf("\nFailure\n\n");
        }
    }

    private void grabeContacts(String state, String city,
                               BufferedWriter contactWriter,
                               BufferedWriter emailWriter,
                               BufferedWriter skippedStateCityWriter) {
        try {
            String searchTarget = city + "," + state;
            grabeContacts(searchTarget, contactWriter, emailWriter, skippedStateCityWriter);
            contactWriter.flush();
            emailWriter.flush();
            skippedStateCityWriter.flush();
            System.out.printf("\t\tNumber of contacts from [Home depot, Lowe's and Ikea]: %d\n", exclusiveCount);
            System.out.printf("\t\tNumber of duplicates eliminated: %d\n", duplicateCount);
            System.out.printf("\t\tTotal number of contacts collected: %d\n", processedEmails.size());
        } catch (IOException e) {
            // TODO: log
            e.printStackTrace();
        }
    }

    private void grabeContacts(String stateCity,
                               BufferedWriter contactWriter,
                               BufferedWriter emailWriter,
                               BufferedWriter skippedStateCityWriter) {
        System.setProperty("webdriver.gecko.driver", PROJ_DIR + "/geckodriver");
        WebDriver driver = null;
        try {
            driver = new FirefoxDriver();
            driver.get("http://www.nkba.org/Design/Homeowners/ProSearch.aspx/");
            WebElement element = driver.findElement(By.id("textbox_prosearch_page"));
            element.clear();
            element.sendKeys(stateCity);
            driver.findElement(
                    By.xpath(
                            "//*[@id=\"dropdown_prosearch_radius_page\"]/option[contains(text(), \"Within 20 Miles\")]"
                    )).click();
            WebElement searchButton = driver.findElement(By.id("button_prosearch_page"));
            searchButton.click();
            try {
                // Wait for the page to load, timeout after 100 seconds
                (new WebDriverWait(driver, 100)).until(ExpectedConditions.elementToBeClickable(
                        By.xpath("//*[@id=\"proSearchFilters\"]/ul/li[1]/a")));

                List<WebElement> results = driver.findElements(By.cssSelector("div[class='result']"));

                for (WebElement result : results) {
                    WebElement header = result.findElement(By.cssSelector("div[class='header']"));
                    String companyName = header.findElement(By.cssSelector("div[class='name']")).getText();

                    WebElement info = result.findElement(By.cssSelector("div[class='info']"));
                    String ownerFullName = info.findElement(By.cssSelector("div[class='left']"))
                                               .findElement(By.cssSelector("h4.ownerName"))
                                               .getText();
                    String category = info.findElement(By.cssSelector("div[class='left']"))
                                          .findElement(By.cssSelector("p"))
                                          .getText();
                    String companyInfo = info.findElement(By.cssSelector("div[class='center']"))
                                             .getText();

                    String address = info.findElement(By.cssSelector("div[class='center']"))
                                         .findElement(By.cssSelector("p"))
                                         .getText();

                    String email = parseContact(companyInfo);

                    // Process valid email only
                    if (!Utils.isValidEmail(email)) {
                        continue;
                    }

                    String phone = extractPhone(companyInfo);
                    String web = extractUrl(companyInfo);
                    String domain = Utils.extractDomainFromEmail(email);

                    if (exclusiveDomains.contains(domain)) {
                        exclusiveCount++;
                    }

                    if (processedEmails.contains(email)) {
                        duplicateCount++;
                    }

                    if (!exclusiveDomains.contains(domain) && !processedEmails.contains(email)) {
                        Contact contact = new Contact(
                                ownerFullName, category, companyName,
                                phone, email, address, web);
                        processedEmails.add(email);
                        emailWriter.write(email + ",");
                        emailWriter.newLine();
                        contactWriter.write(contact.toString());
                        contactWriter.newLine();
                    }
                }
            } catch (Exception e) {
                // TODO: log
                e.printStackTrace();
                if (driver != null) {
                    try {
                        skippedStateCityWriter.write("\"" + stateCity + "\"" + ",");
                        skippedStateCityWriter.newLine();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                    System.out.printf("\n**** Ooops!, driver error! Skipped %s\n", stateCity);
                    driver.quit();
                    driver = null;
                }
            }
        } finally {
            if (driver != null) {
                driver.quit();
            }
        }
    }

    private String parseContact(String contact) {
        String emailRegex = "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9.-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(contact);

        while (matcher.find()) {
            return matcher.group();
        }
        return "N/A";
    }

    private String extractPhone(String contact) {
        String emailRegex = "\\([1-9][0-9]*\\)\\s+\\d{3}-\\d{4}";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(contact);

        while (matcher.find()) {
            return matcher.group();
        }
        return "N/A";
    }

    private String extractUrl(String contact) {
        ArrayList<String> links = new ArrayList();

        String regex = "\\(?\\b(http://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(contact);
        while (m.find()) {
            String urlStr = m.group();
            if (urlStr.startsWith("(") && urlStr.endsWith(")")) {
                urlStr = urlStr.substring(1, urlStr.length() - 1);
            }
            links.add(urlStr);
        }
        return links.isEmpty() ? "N/A" : links.get(0);
    }
}
