package com.bgitechgroup.tool.util;


import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

public class Utils {
    final static String PROJ_DIR = System.getProperty("user.dir");
    final static String OUTPUT_DIR = "output";
    final static String FILE_PREFIX = "nkba-contacts";

    public static String generateFileName(String state) {
        StringBuilder resutl = new StringBuilder(PROJ_DIR);
        resutl.append("/").append(OUTPUT_DIR)
              .append("/").append(FILE_PREFIX)
              .append("-").append(state);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String timestamp = dateFormat.format(new Date());
        resutl.append("-").append(timestamp)
              .append(".csv");

        return resutl.toString();
    }

    public static String extractDomainFromEmail(String email) {
        String[] tokens = email.split("@");
        return tokens.length == 2 ? tokens[1] : "N/A";
    }

    public static Map<String, Map<String, Set<String>>> buildStateZipcodeMap(String filename) {
        Map<String, Map<String, Set<String>>> result = new HashMap<>();
        try (Stream<String> stream = Files.lines(Paths.get(filename))) {
            stream.forEach(line -> {
                String[] tokens = line.split(",");
                String zipcode = tokens[0];
                String city = tokens[1];
                String state = tokens[2];
                result.putIfAbsent(state, new HashMap<>());
                result.get(state).putIfAbsent(city, new HashSet<>());
                result.get(state).get(city).add(zipcode);
            });
        } catch (IOException e) {
            // TODO: log
            e.printStackTrace();
        }
        return result;
    }

    public static Set<String> buildSetFromCvs(String filename) {
        Set<String> result = new HashSet<>();
        try (Stream<String> stream = Files.lines(Paths.get(filename))) {
            stream.forEach(line -> {
                String[] tokens = line.split(",");
                for (String token : tokens) {
                    token = token.trim();
                    if (!token.isEmpty()) {
                        result.add(token);
                    }
                }
            });
        } catch (IOException e) {
            // TODO: log
            e.printStackTrace();
        }
        return result;
    }

    public static String normalize(String s) {
        s = s.trim();
        if (s.isEmpty()) {
            return "N/A";
        }

        if (s.contains(",")) {
            if (s.contains("\"")) {
                return s.replace(",", "|");
            }
            return "\"" + s + "\"";
        }
        return s;
    }

    public static boolean isValidEmail(String email) {
        boolean isValid = false;
        try {
            // Create InternetAddress object and validated the supplied
            // address which is this case is an email address.
            InternetAddress internetAddress = new InternetAddress(email);
            internetAddress.validate();
            isValid = true;
        } catch (AddressException e) {
            // TODO: log
            System.out.println("You are in catch block -- Exception Occurred for: " + email);
        }
        return isValid;
    }

    public static String makeContactHeader() {
        StringBuilder header = new StringBuilder();
        header.append("OwnerFullName")
              .append(",OwnerFirstName")
              .append(",OwnerLastName")
              .append(",Category")
              .append(",CompanyName")
              .append(",Phone")
              .append(",Email")
              .append(",Address")
              .append(",Web");

        return header.toString();
    }
}

